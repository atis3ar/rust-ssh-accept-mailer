use std::io::Write;
use std::io;
use atty;
use atty::Stream;

#[derive(Debug)]
pub struct EnvVars {
    pub slack_token: String,
    pub slack_channel: String,
    pub slack_user_name: String,
}

impl EnvVars {
    pub fn get_input(var_name: &str) -> String {
        if atty::isnt(Stream::Stdin) {
            panic!("Stdin is not a terminal and vars can not be input");
        }
        let mut value = String::new();
        print!("Please input {}: ", var_name);
        io::stdout().flush().unwrap();
        io::stdin().read_line(&mut value)
            .expect("Did not enter a correct string");
        return value.trim().to_string();
    }

    pub fn load() -> EnvVars {
        println!(
            "Loading env vars: {:?}",
            ["SLACK_TOKEN", "SLACK_CHANNEL", "SLACK_USER"]
        );

        let mut instance = EnvVars {
            slack_token: String::new(),
            slack_channel: String::new(),
            slack_user_name: String::new(),
        };

        if std::env::var("SLACK_TOKEN").is_ok() {
            instance.slack_token = std::env::var("SLACK_TOKEN").unwrap().to_string();
        } else {
            instance.slack_token = EnvVars::get_input("slack token");
            std::env::set_var("SLACK_TOKEN", &instance.slack_token);
        }

        if std::env::var("SLACK_CHANNEL").is_ok() {
            instance.slack_channel = std::env::var("SLACK_CHANNEL").unwrap().to_string();
        } else {
            instance.slack_channel = EnvVars::get_input("slack channel");
            std::env::set_var("SLACK_CHANNEL", &instance.slack_channel);
        }

        if std::env::var("SLACK_USER").is_ok() {
            instance.slack_user_name = std::env::var("SLACK_USER").unwrap().to_string();
        } else {
            instance.slack_user_name = EnvVars::get_input("slack user");
            std::env::set_var("SLACK_USER", &instance.slack_user_name);
        }

        println!("Loaded env vars: {:?}", instance);
        return instance;
    }
}