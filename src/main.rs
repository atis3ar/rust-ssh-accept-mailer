#![allow(dead_code)]

extern crate regex;
extern crate dirs;
extern crate reqwest;

mod systemd_installer;
mod history;
mod slack;
mod env;

use std::process::{Command, Stdio, exit};
use std::io::{BufRead, BufReader};
use regex::Regex;
use std::{fs};
use history::History;
use env::EnvVars;
use std::path::Path;

const LOG_PATH: &str = "/var/log/auth.log";


fn main() {
    for arg in std::env::args() {
        if arg == "--install" {
            systemd_installer::install();
            exit(0);
        } else if arg == "--uninstall" {
            systemd_installer::uninstall();
            exit(0);
        }
    }

    let _user_conf_dir = dirs::config_dir().unwrap();
    let user_conf_dir = _user_conf_dir.as_os_str().to_str().unwrap();
    let history_file = format!("{}/com_atiskr/rust_ssh_mailer/history.csv", user_conf_dir);
    let conf_dir = format!("{}/com_atiskr/rust_ssh_mailer", user_conf_dir);
    fs::create_dir_all(conf_dir.as_str()).unwrap();

    println!("{}", history_file);

    let env_vars = EnvVars::load();
    let mut history = History::new(history_file);
    let pattern = Regex::new(r"(.*? \d+ \d+:\d+:\d+) .*? sshd\[\d+]: Accepted (.*?) for (.*?) from (.*?) ").unwrap();

    if !Path::new(LOG_PATH).exists() {
        panic!("Log file {} does not exist", LOG_PATH);
    }

    let handle = Command::new("tail")
        .args(&["-F", LOG_PATH, "-n", "+1"])
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn();

    match handle {
        Ok(child) => {
            println!("Tail started");

            let mut i: i64 = 0;
            let reader = BufReader::new(child.stdout.unwrap());

            reader.lines()
                .filter_map(|line| line.ok())
                .for_each(|line| {
                    let _captures = pattern.captures(line.as_str());
                    let captures;

                    if _captures.is_some() {
                        captures = _captures.unwrap();

                        if captures.len() >= 5 {
                            let date = &captures[1];
                            let accept_method = &captures[2];
                            let user_name = &captures[3];
                            let ip_addr = &captures[4];
                            println!(
                                "{}: Accept: {} | {} | {} | {}",
                                i,
                                date,
                                accept_method,
                                user_name,
                                ip_addr
                            );

                            if history.find(
                                Option::from(user_name),
                                Option::from(ip_addr),
                            ).is_none() {
                                history.append(date, user_name, ip_addr);
                                let res = slack::send_message(
                                    &env_vars.slack_token,
                                    &env_vars.slack_channel,
                                    &env_vars.slack_user_name,
                                    format!(
                                        "New sign in at \"{}\" from \"{}\" - \"{}\"",
                                        date,
                                        ip_addr,
                                        user_name
                                    ).as_str(),
                                );

                                if res.is_err() {
                                    println!("Error sending slack");
                                }
                            }
                        }
                    }

                    i += 1;
                });
        }

        Err(error) => {
            panic!("ERR: {}", error);
        }
    }
}
