use std::fs;
use std::path::Path;
use crate::env::EnvVars;
use whoami;

const SERVICE_TEMPLATE: &'static str = include_str!("resources/systemd_service.jinja2");
const SYSTEMD_SERVICE_PATH: &'static str = "/etc/systemd/system/com.atiskr.dev_stats2.service";
const EXECUTABLE_PATH: &'static str = "/opt/com_atiskr/rust-ssh-mailer";

fn format_service_template(
    user_name: &str,
    file_path: &str,
    env_vars: &EnvVars,
) -> String {
    return String::from(SERVICE_TEMPLATE)
        .replace("{{ user }}", user_name)
        .replace("{{ exec_start }}", file_path)
        .replace("{{ slack_token }}", &env_vars.slack_token)
        .replace("{{ slack_channel }}", &env_vars.slack_channel)
        .replace("{{ slack_user }}", &env_vars.slack_user_name);
}

pub fn install() {
    let env_vars = EnvVars::load();
    fs::create_dir_all("/opt/com_atiskr").unwrap();
    fs::copy(std::env::current_exe().unwrap(), EXECUTABLE_PATH).unwrap();
    println!("Copying executable to {}", EXECUTABLE_PATH);

    let template = format_service_template(
        whoami::username().as_str(),
        EXECUTABLE_PATH,
        &env_vars,
    );

    println!("Installing systemd service");
    fs::write(SYSTEMD_SERVICE_PATH, template).unwrap();

    // TODO: call after : systemctl restart com.atiskr.dev_stats2.service
}

pub fn uninstall() {
    let paths_to_remove = vec![SYSTEMD_SERVICE_PATH, EXECUTABLE_PATH];
    for path in paths_to_remove.iter() {
        match fs::remove_file(path) {
            Ok(_x) => { println!("Removed {}", path); }
            Err(err) => { println!("ERR: removing {} . {}", path, err); }
        }
    }
}

pub fn is_installed() -> bool {
    let paths = vec![SYSTEMD_SERVICE_PATH, EXECUTABLE_PATH];
    for path in paths {
        if !Path::new(path).exists() {
            return false;
        }
    }
    return true;
}