use std::io::Write;
use std::vec::Vec;

#[derive(Debug)]
pub struct HistoryItem {
    date: String,
    ip_addr: String,
    user_name: String,
}

#[derive(Debug)]
pub struct History {
    path: String,
    items: Vec<HistoryItem>,
}

impl History {
    pub fn new(path: String) -> History {
        let mut instance = History {
            path,
            items: Vec::new(),
        };

        println!("Read file");
        let contents = std::fs::read_to_string(&instance.path);
        if contents.is_ok() {
            for line in contents.unwrap().lines() {
                let parts: Vec<&str> = line.split(",").collect();
                if parts.len() < 3 {
                    println!("Invalid part count: {}: {:?}", parts.len(), parts);
                    continue;
                }

                let item = HistoryItem {
                    date: String::from(parts[0]),
                    user_name: String::from(parts[1]),
                    ip_addr: String::from(parts[2]),
                };

                instance.items.push(item);
            }
        }

        return instance;
    }

    pub fn find(&self, user_name: Option<&str>, ip_addr: Option<&str>) -> Option<&HistoryItem> {
        for item in self.items.iter() {
            if user_name.is_some()
                && ip_addr.is_some()
                && item.user_name == user_name.unwrap()
                && item.ip_addr == ip_addr.unwrap() {
                return Option::from(item);
            }

            if user_name.is_some()
                && ip_addr.is_none()
                && item.user_name == user_name.unwrap() {
                return Option::from(item);
            }

            if ip_addr.is_some()
                && user_name.is_none()
                && item.ip_addr == ip_addr.unwrap() {
                return Option::from(item);
            }
        }
        return None;
    }

    pub fn append(&mut self, date: &str, user_name: &str, ip_addr: &str) -> &History {
        self.items.push(HistoryItem {
            date: date.to_string(),
            user_name: user_name.to_string(),
            ip_addr: ip_addr.to_string(),
        });

        println!("Append history: {:?}", self.items.last());

        let file = std::fs::OpenOptions::new()
            .append(true)
            .create(true)
            .open(&self.path);

        match file {
            Ok(mut file) => {
                let result = file.write_fmt(format_args!(
                    "{},{},{}\n",
                    date,
                    user_name,
                    ip_addr
                ));

                if result.is_err() {
                    println!("Failed to append history");
                }
            }

            Err(error) => {
                println!("Failed to open history file for writing with error: {}", error);
            }
        }
        return self;
    }
}