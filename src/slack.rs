use reqwest::header::{HeaderValue, HeaderMap};
use std::collections::HashMap;
use reqwest::StatusCode;


pub fn send_message(
    token: &str,
    channel: &str,
    user_name: &str,
    message: &str,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut headers = HeaderMap::new();
    headers.append(
        "Authorization",
        HeaderValue::from_str(format!(
            "Bearer {}", token
        ).as_str()).unwrap(),
    );

    headers.append(
        "User-Agent",
        HeaderValue::from_str("rust-reqwest-client-test0701").unwrap(),
    );

    let params: HashMap<&str, &str> = [
        ("channel", channel),
        ("username", user_name),
        ("text", message),
    ].iter()
        .cloned()
        .collect();

    let client = reqwest::blocking::Client::new();
    let response = client.post("https://slack.com/api/chat.postMessage")
        .headers(headers)
        .json(&params)
        .send()
        .unwrap();

    if response.status() != StatusCode::OK {
        println!("Bad response status: {}", response.status().to_string());
        println!("{:?}", response.text());
    }
    Ok(())
}